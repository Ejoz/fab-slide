# Fab Slide
_Machine Building Week at Fab Academy_

We decided to build a camera slider. The all documentation can be found on the [group project page](http://fabacademy.org/2022/labs/sorbonne/machine-week/machine-week/).

- Stéphane: Electronics
- Chloé: Mechanics
- Clara: Phone holder + Camera support

# Phone Holder (Clara)
[Head ball](https://www.thingiverse.com/thing:1165406)

Additional material needed:
- M6 Hex Bolt 
- M6 Nut
- M4 x 50mm Hex Bolt 
- M4 Nut 

We used M5 bolt and nut because we had no M4 bolt long enough.

Universal phone holder :
https://www.thingiverse.com/thing:2505243